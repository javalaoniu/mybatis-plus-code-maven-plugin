# mybatis-plus代码生成工具
本工具基于MyBatis-Plus的代码生成器， MyBatis-Plus官网：https://baomidou.com/ ，请尊重原作者作品。


本项目开发环境： windows10+openjdk17.0.2+maven3.6.3+IntelliJ IDEA 2024.1 (Community Edition)+mysql8.0.35

代码生成器自身版本：3.2.0，为什么不用新版（3.5.6）？其实新版的在代码生成上做了很大的优化，使用更加方便了，有兴趣的同学可以尝试官方新版。我不用新版主要是我这个项目原来就是基于3.2.0版本修改的，主要修改了模板的内容，我使用了官方的新版发现生成的代码不是很符合我的风格，所以才自己修改了模板并增加了一些其他配置，使得生成的代码项目可以直接运行，虽然插件是3.2.0的版本，但是生成的项目代码是新版的，正如我下面所说。
主要的依赖如下：
```xml
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-generator</artifactId>
    <version>3.2.0</version>
</dependency>

<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>8.0.28</version>
    <scope>runtime</scope>
</dependency>

<dependency>
    <groupId>org.freemarker</groupId>
    <artifactId>freemarker</artifactId>
    <version>2.3.28</version>
</dependency>

```





## 使用教程
### 1. 拉取本项目

`git clone -b springboot3.2.2+mybatisplus3.5.6+knife4j git@gitee.com:javalaoniu/mybatis-plus-code-maven-plugin.git`

### 2. 执行安装命令
`mvn clean install`

把该项目（执行命令后会安装为maven插件）部署到本地maven库，后面就可以在其他项目中引用该插件，引用该插件，打开你的项目的pom.xml文件，添加如下内容，然后修改相应的配置参数
```xml
<plugin>
    <groupId>io.gitee.javalaoniu</groupId>
    <artifactId>mybatis-plus-code-maven-plugin</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <configuration>
        <!--代码类中的author，默认为空-->
        <!--<author>zhangsan</author>-->
        <!--生成代码保存路径，默认保存在项目目录的target目录下-->4
        、
        <savePath>D:\Workspace\mympdemo</savePath>
        <!--项目（模块）名，也是包名的一部分（com.example.demotest）-->
        <projectName>demotest</projectName>
        <!--每次生成的文件覆盖已有文件，谨慎使用。默认为false（不覆盖）-->
        <!--<fileOverride>true</fileOverride>-->
        <!--配置swagger3 默认是关闭的-->
        <openApi3>true</openApi3>

        <dbSetting>
            <!--数据库类型，默认mysql，其他库没测试过-->
            <!--<type>mysql</type>-->
            <!--数据库驱动类，默认是com.mysql.cj.jdbc.Driver-->
            <!--<driverClass>com.mysql.cj.jdbc.Driver</driverClass>-->
            <url>jdbc:mysql://localhost:3306/test?useUnicode=true&amp;characterEncoding=utf8&amp;serverTimezone=GMT%2B8&amp;tinyInt1isBit=false</url>
            <username>root</username>
            <pwd>root123456</pwd>
            <!--数据库schema，一般是oracle才有，mysql不用设置-->
            <!--<schema>dbschema</schema>-->
            <!--表面的前缀，不填可以留空-->
            <tablePrefix>t_</tablePrefix>
            <tables>t_sys_role,t_sys_user</tables>
        </dbSetting>

        <!--
        controller、service、mapper、entity包名（文件夹）设置，这些类最终包名（groupPackage+moduleName+自己），下面设置和默认设置一样
        xml为mybatis的xml文件存放文件夹（默认放在resources下）
        -->
        <packageSetting>
            <!--父包名，一般设置为域名，最终包名会用$parent+$module，试试就知道了-->
            <parent>com.baidu</parent>
            <entity>entity</entity>
            <mapper>mapper</mapper>
            <service>service</service>
            <serviceImpl>service.impl</serviceImpl>
            <controller>controller</controller>
            <xml>mapper</xml>
        </packageSetting>

        <!--
        策略配置，一般不用配置，除非很特殊，必须要配置
        entitySuperClass： 自己的实体类的父类全路径，实体类po会直接继承改类，一般不设置；
        controllerSuperClass： 自己的controller类的父类全路径，controller类会直接继承该类，一般不设置
        -->
        <!--
        <strategySetting>
            <entitySuperClass>aa.MyEntityParent</entitySuperClass>
            <controllerSuperClass>aa.MyControllerParent</controllerSuperClass>
        </strategySetting>
        -->
    </configuration>
</plugin>
```
### 3. 执行生成代码命令
![img.png](imgs/mybatis-plus-code-generate-img-1.png)

或者使用命令方式，打开命令控制台并进入到pom文件所在文件夹，再执行插件命令：

`mvn mybatis-plus-code:generate`

![img.png](imgs/mybatis-plus-code-generate-img-2.png)

### 4、用idea打开生成的项目
![img.png](imgs/mybatis-plus-code-generate-img-3.png)

### 5、修改你的数据库
其实这一步是不用配置的，默认会使用插件中配置的数据库信息。如果不是该数据库则需要修改。
打开src/main/resources/application-mp.yml文件，配置你的数据库信息
![img.png](imgs/step5.png)

### 6、运行生成的项目
![img.png](imgs/mybatis-plus-code-generate-img-4.png)



## 生成的项目介绍
使用本工具生成的项目特点：
主要技术框架springboot3.2.2+mybatis-plus3.5.6+swagger3(knife4j)，实现数据库单表增删改查功能，并实现批量插入更新删除功能，项目的功能和配置都是实践出的最佳配置，生成的代码即可直接运行使用。
> 想生成的代码为springboot2.5.4+mybatis-plus3.5.3.2+hikari+swagger2 2.9.2技术组合，可以拉另外一个分支（springboot2.5.4+mybatis-plus3.5.3.2+swagger2.9.2）

主要依赖的maven包：
```xml
<spring-boot.version>3.2.2</spring-boot.version>
<mybatis-plus.version>3.5.6</mybatis-plus.version>
<knife4j.version>4.5.0</knife4j.version>

<!-- SpringBoot的依赖配置-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-dependencies</artifactId>
    <version>${spring-boot.version}</version>
    <type>pom</type>
    <scope>import</scope>
</dependency>

<!--mybatis start-->
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-spring-boot3-starter</artifactId>
    <version>${mybatis-plus.version}</version>
</dependency>
<!--mybatis end-->

<!--openapi3 start-->
<dependency>
    <groupId>com.github.xiaoymin</groupId>
    <artifactId>knife4j-openapi3-jakarta-spring-boot-starter</artifactId>
    <version>${knife4j.version}</version>
</dependency>
<!--openapi3 end-->
```
### 目录说明
- AppApplication为项目启动入口类
- aop文件夹存放切面相关配置；
- config文件夹存放项目配置类，主要包含MybatisPlusConfig配置类，旧项目可以不用拷贝该文件夹及文件；
- consts文件夹存放常量定义的类和枚举类；
- exception文件夹存放自定义异常类；
- controller文件夹存放项目控制器类，对应表的增删改查、批量操作等接口；
- service文件夹存放业务接口类；
- service.impl文件夹存放service的实现类，类中部分字段如果表中没有需要自己手动修改；
- mapper文件夹存放dao数据操作类，主要是myabtis的接口类文件，实现由mybatis生成；
- entity文件夹存放实体类，po对应数据库表字段、vo对应前端（model）、dto为数据传输对象（DTO类，多用于处理po和vo之间的关系，大致有时候也可以和vo相同。PO接收数据库的数据，然后转成DTO，DTO再转成VO（有时候为了偷懒，直接把DTO当成VO就不再转一层直接返回给前端）。在我的项目中没有使用dto对象，直接使用了vo对象和po进行转换，很多时候并不会区分那么多，不需要在意这个）；
- utils文件夹存放工具类；


- resources中的mapper存放mybatis对应的xml文件；
- resources中的application.yml项目配置文件（最先加载）；
- resources中的application-mp.yml是mybatis配置文件；
- resources中的logback-spring.xml是日志配置文件；
- 生成项目名首字母的ico图片，存放在resources/static/favicon.ico，请自行修改为自己的；


- imgs文件存放readme文档的图片；

**其他特点**：
1. hikari为springboot推荐的数据库连接池，据说性能还可以，springboot默认使用的就是这个；
2. json格式化主要使用jackson框架，并处理时间返回格式问题，该框架也是springboot推荐，个人不喜欢fastjson、gson；
   多环境配置；
3. 统一异常捕获；
4. 统一返回对象；
5. 日志打印根据spring.profiles.active: xxx配置自动使用相应的日志打印，并且配合在各环境文件中配置logging开关或者打印级别，更好的控制日志打印；
6. wagger3 api文档，使用knife4j-openapi3-jakarta-spring-boot-starter版本，简化配置并美化页面效果，可以直接根据配置是否生效，并且有多一套美化皮肤，可以看原始swagger文档也可以看美化后的文档；
   访问地址：
    - swagger 原始皮肤访问地址:http://127.0.0.1:8080/swagger-ui.html
    - swagger 优化皮肤访问地址:http://127.0.0.1:8080/doc.html




## demo
[mybatis-code-maven-plugin-demo](https://gitee.com/javalaoniu/mybatis-code-maven-plugin-demo)
