# ${cfg.module}
本项目主要技术框架springboot3.2.2+mybatis-plus3.5.6+swagger3(knife4j)，实现数据库单表增删改查功能，并实现批量插入更新删除功能，项目的功能和配置都是实践出的最佳配置，生成的代码即可直接运行使用。

## 介绍
${cfg.module} 项目介绍，对于新项目可以直接拷贝整个项目代码当作project的一个模块直接运行，对于旧项目可以拷贝mybatis相关类文件到适当的目录。

## 项目说明
- AppApplication为项目启动入口类
- aop文件夹存放切面相关配置；
- config文件夹存放项目配置类，主要包含MybatisPlusConfig配置类，旧项目可以不用拷贝该文件夹及文件；
- consts文件夹存放常量定义的类和枚举类；
- exception文件夹存放自定义异常类；
- controller文件夹存放项目控制器类，对应表的增删改查、批量操作等接口；
- service文件夹存放业务接口类；
- service.impl文件夹存放service的实现类，类中部分字段如果表中没有需要自己手动修改；
- mapper文件夹存放dao数据操作类，主要是myabtis的接口类文件，实现由mybatis生成；
- entity文件夹存放实体类，po对应数据库表字段、vo对应前端（model）、dto为数据传输对象（DTO类，多用于处理po和vo之间的关系，大致有时候也可以和vo相同。PO接收数据库的数据，然后转成DTO，DTO再转成VO（有时候为了偷懒，直接把DTO当成VO就不再转一层直接返回给前端）。在我的项目中没有使用dto对象，直接使用了vo对象和po进行转换，很多时候并不会区分那么多，不需要在意这个）；
- utils文件夹存放工具类；


- resources中的mapper存放mybatis对应的xml文件；
- resources中的application.yml项目配置文件（最先加载）；
- resources中的application-mp.yml是mybatis配置文件；
- resources中的logback-spring.xml是日志配置文件；
- 生成项目名首字母的ico图片，存放在resources/static/favicon.ico，请自行修改为自己的；


- imgs文件存放readme文档的图片；



**项目其他特点：**
- hikari为springboot推荐的数据库连接池，据说性能还可以；
- json格式化主要使用jackson框架，并处理时间返回格式问题，该框架也是springboot推荐，个人不喜欢fastjson、gson；
- 多环境配置；
- 全局统一异常捕获；
- 统一返回对象；
- 日志使用spring的日志starter（spring-boot-starter-logging），并配置了多环境日志文件，日志打印根据spring.profiles.active: xxx配置自动使用相应的日志打印，并且配合在各环境文件中配置logging开关或者打印级别，错误日志单独打印到日志文件，更好的控制日志打印；
- swagger3 api文档，使用knife4j-openapi3-jakarta-spring-boot-starter版本，简化配置并美化页面效果，可以直接根据配置是否生效，并且有多一套美化皮肤，可以看原始swagger文档也可以看美化后的文档；
    访问地址：
    - swagger 原始皮肤访问地址:http://127.0.0.1:8080/swagger-ui.html
    - swagger 优化皮肤访问地址:http://127.0.0.1:8080/doc.html


## 复制说明
正常情况下，当作新项目可以整个代码当作新项目用；
如果已有项目的情况，必须复制的有resources中的mapper、mapper、entity三个文件夹中的文件，这是mybatis最基本的，直接拷贝这些文件到已有项目即可直接使用，避免手写错误问题，当然得是已有项目本身是整合好mybatis的，
其他的工具类、controller、service等自己看着办就好了。

## 其他文档参考
MyBatis-Plus使用指南官网：https://baomidou.com/，mybatis-plus最全文档。

## 运行问题
1. 请求接口的时候如果有以下报错：Caused by: java.lang.reflect.InaccessibleObjectException: Unable to make protected native java.lang.Object java.lang.Object.clone() throws java.lang.CloneNotSupportedException accessible: module java.base does not "opens java.lang" to unnamed module @76329302

解决方法，在jdk vm参数中添加 --add-opens java.base/java.lang=ALL-UNNAMED
<img src="imgs/img_q1.png" width = "800px" />

> 使用本插件生成的代码并非无bug，仅供学习使用。如用于商业环境造成损失和本人无关。


