package ${cfg.utilPackage};

import java.lang.reflect.Field;

/**
* 对象属性工具类，比较简单
*/
public class BeanUtil {

    /**
    * 相同对象合并，将原对象的非空属性的值赋值给目标对象
    * @param origin 源对象
    * @param destination 目标对象
    * @param <T> 对象的类型
    */
    public static <T> void merge(T origin, T destination) throws IllegalAccessException {
        if (origin == null || destination == null){
            return;
        }
        if (!origin.getClass().equals(destination.getClass())){
            return;
        }

        Field[] fields = origin.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            Object value = field.get(origin);
            if (null != value) {
                field.set(destination, value);
            }
            field.setAccessible(false);
        }
    }
}