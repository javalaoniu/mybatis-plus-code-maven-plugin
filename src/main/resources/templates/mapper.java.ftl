package ${package.Mapper};

import ${package.Entity}.${entity};
import ${superMapperClassPackage};

import java.util.List;

/**
 * <p>
 * ${table.comment!} 数据层，mybatis接口类
 * </p>
 *
 * @createTime: ${cfg.createTime}
 */
<#if kotlin>
interface ${table.mapperName} : ${superMapperClass}<${entity}>
<#else>
public interface ${table.mapperName} extends ${superMapperClass}<${entity}> {
    // Available parameters are [sub${entity}s, collection, list]
    // 参数是list集合时，在xml文件的for循环中可以用sub${entity}s, collection, list接收
    // 参数是set集合时，在xml文件的for循环中可以用sub${entity}s, collection接收
    void batchInsert(List<${entity}> sub${entity}s);

    void batchInsertOnDuplicateKeyUpdate(List<${entity}> sub${entity}s);

    void batchDelete(List<${entity}> sub${entity}s);

    void batchModify(List<${entity}> sub${entity}s);
}
</#if>
