package ${cfg.constsPackage};

/**
 * 业务异常标识码
 */
public enum AppErrorCode {

    AppException(100500999, "业务主动抛出异常"),
    Exception(100500000, "服务器端未知错误"),

    MissingServletRequestParameterException(100400001, "缺少请求参数"),
    HttpMessageNotReadableException(100400002, "参数解析失败"),
    MethodArgumentNotValidException(100400003, "参数校验失败"),
    BindException(100400004, "参数绑定失败"),
    ValidationException(100400005, "参数验证失败"),

    NoHandlerFoundException(100404001, "请求的资源不存在"),

    HttpRequestMethodNotSupportedException(100405001, "方法不被允许"),

    HttpMediaTypeNotSupportedException(100415001, "不支持的内容类型"),

    DataIntegrityViolationException(100500001, "操作数据库出现异常"),
    MethodArgumentTypeMismatchException(100500002, "服务器内部错误，方法参数类型不匹配");

    private int code;
    private String desc;

    AppErrorCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
