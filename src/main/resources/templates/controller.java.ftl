package ${package.Controller};

<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>
import ${cfg.commonPackage}.PageParams;
import ${cfg.commonPackage}.PageResult;
import ${cfg.commonPackage}.AppResp;
import ${cfg.voEntityPackage}.${entity}VO;
import ${package.Service}.${entity}Service;
<#if cfg.swagger3>
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
</#if>
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

<#assign entityIdType = "Long">
<#assign entityIdName = "Id">
<#list table.fields as field>
    <#if field.keyFlag>
    <#assign entityIdType = field.columnType>
    <#assign entityIdName = field.propertyName>
    </#if>
</#list>
/**
 * <p>
 * <#if (table.comment)?trim?length gt 1>${table.comment}<#else>${entity}</#if> 控制层
 * </p>
 *
 * @createTime: ${cfg.createTime}
 */
<#if cfg.swagger3>
@Tag(name = "<#if (table.comment)?trim?length gt 1>${table.comment}<#else>${entity}</#if>控制器类")
</#if>
@Validated
@RestController("${'${table.controllerName}'?uncap_first}4${cfg.module}")
@RequestMapping("${cfg.requestMappingUrlPrefix}/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>

    @Autowired
    private ${table.serviceName} ${'${entity}Service'?uncap_first};

    <#if cfg.swagger3>
    @Operation(summary = "查询列表分页")
    </#if>
    @PostMapping("/page")
    public AppResp<PageResult<${entity}VO>> page(@RequestBody PageParams<${entity}VO> pageParams){
        return ${'${entity}Service'?uncap_first}.page(pageParams);
    }

    <#if cfg.swagger3>
    @Operation(summary = "根据记录Id查询",
        parameters = {
            @Parameter(name = "id", description = "记录id", required = true, in = ParameterIn.PATH)
        }
    )
    </#if>
    @GetMapping("")
    public AppResp<${entity}VO> queryById(@RequestParam(name = "id") ${'${entityIdType}'?capitalize} ${'${entity}'?uncap_first}${'${entityIdName}'?cap_first}){
        return ${'${entity}Service'?uncap_first}.queryById(${'${entity}'?uncap_first}Id);
    }

    <#if cfg.swagger3>
    @Operation(summary = "添加记录")
    </#if>
    @PostMapping("")
    public AppResp<Void> add(@Validated @RequestBody ${entity}VO ${'${entity}'?uncap_first}VO){
        return ${'${entity}Service'?uncap_first}.add(${'${entity}'?uncap_first}VO);
    }

    <#if cfg.swagger3>
    @Operation(summary = "删除记录",
       parameters = {
           @Parameter(name = "id", description = "记录id", required = true, in = ParameterIn.PATH)
       }
   )
    </#if>
    @DeleteMapping("")
    public AppResp<Void> delById(@RequestParam(name = "id") ${'${entityIdType}'?capitalize} ${'${entity}'?uncap_first}Id){
        return ${'${entity}Service'?uncap_first}.delById(${'${entity}'?uncap_first}Id);
    }

    <#if cfg.swagger3>
    @Operation(summary = "修改记录")
    </#if>
    @PutMapping("")
    public AppResp<Void> modify(@Validated @RequestBody ${entity}VO ${'${entity}'?uncap_first}VO){
        return ${'${entity}Service'?uncap_first}.modify(${'${entity}'?uncap_first}VO);
    }

    @PostMapping("/batch-insert")
    public AppResp<Void> batchInsert(@RequestBody List<${entity}VO> ${'${entity}'?uncap_first}Vos) {
        return ${'${entity}Service'?uncap_first}.batchAdd(${'${entity}'?uncap_first}Vos);
    }

    @PostMapping("/batch-update")
    public AppResp<Void> batchUpdate(@RequestBody List<${entity}VO> ${'${entity}'?uncap_first}Vos) {
        return ${'${entity}Service'?uncap_first}.batchModify(${'${entity}'?uncap_first}Vos);
    }

    @PostMapping("/batch-delete")
    public AppResp<Void> batchDelete(@RequestBody List<${entity}VO> ${'${entity}'?uncap_first}Vos) {
        return ${'${entity}Service'?uncap_first}.batchDelete(${'${entity}'?uncap_first}Vos);
    }
}