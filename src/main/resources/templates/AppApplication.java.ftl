package ${cfg.appPackage};

import ${cfg.utilPackage}.SpringContextUtil;
import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

/**
 * 入口类
 * @createTime: ${cfg.createTime}
 */
@SpringBootApplication
public class AppApplication {


    /**
     * Main method, This is just a standard method that follows the Java convention for an application entry point.
     *
     * @param args args
     */
    public static void main(String[] args) {
        // TimeZone.setDefault(TimeZone.getTimeZone("GMT"));

        ApplicationContext ctx = new SpringApplicationBuilder().sources(AppApplication.class/*, CoreApplication.class*/).run(args);

        SpringContextUtil.setApplicationContext(ctx);
<#if cfg.swagger3>
        showSwaggerApi();
</#if>
    }

<#if cfg.swagger3>
    private static void showSwaggerApi() {
        String port = SpringContextUtil.getProperty("server.port");
        System.out.println("swagger 原始皮肤访问地址:http://127.0.0.1:" + port + "/swagger-ui.html");
        System.out.println("swagger 优化皮肤访问地址:http://127.0.0.1:" + port + "/doc.html");
    }
</#if>

    /**
     * 配置多语言
     *
     * @return
     */
    @Bean
    public LocaleResolver localeResolver() {
        CookieLocaleResolver slr = new CookieLocaleResolver();
        slr.setCookieMaxAge(3600);

        //设置存储的Cookie的name为Language
        slr.setCookieName("Language");
        return slr;
    }

    @Bean
    public TomcatServletWebServerFactory servletContainer() {
        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();
        tomcat.addConnectorCustomizers(new MyTomcatConnectorCustomizer());
        return tomcat;
    }

    static class MyTomcatConnectorCustomizer implements TomcatConnectorCustomizer {
        @Override
        public void customize(Connector connector) {
            Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
            //设置最大连接数
            protocol.setMaxConnections(2000);
            //最大线程数
            protocol.setMaxThreads(2000);
            //初始线程数，最小空闲线程数
            protocol.setMinSpareThreads(40);
            //当处理不过来时，允许的队列长度
            protocol.setAcceptCount(800);
            //超时时间
            protocol.setConnectionTimeout(20000);
        }
    }

}
