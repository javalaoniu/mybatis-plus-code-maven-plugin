spring:
  datasource:
    url: ${cfg.jdbcUrl}
    username: ${cfg.jdbcUser}
    password: ${cfg.jdbcPwd}
    driver-class-name: com.mysql.cj.jdbc.Driver
    tomcat:
      max-active: 200
      max-wait: 10000
      test-on-borrow: true
    hikari: # Hikari 数据源专用配置
      maximum-pool-size: 20
      minimum-idle: 5

# 配置mybatis-plus打印日志
mybatis-plus:
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl