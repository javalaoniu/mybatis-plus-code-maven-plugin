package ${cfg.utilPackage};

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.Type;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

/**
 * bean实体拷贝工具类，主要是对orika开源组件的封装
 */
public class OrikaBeanMapper {
    private static final MapperFacade MAPPER_FACADE;

    static {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().useAutoMapping(true).mapNulls(true).build();
        mapperFactory.getConverterFactory().registerConverter(new TrimConverter());
        mapperFactory.getConverterFactory().registerConverter(new LocalDateTimeConverter());
        mapperFactory.getConverterFactory().registerConverter(new LocalDateConverter());
        mapperFactory.getConverterFactory().registerConverter(new LocalTimeConverter());
        MAPPER_FACADE = mapperFactory.getMapperFacade();
    }


    public static <S, D> void map(S from, D to) {
        MAPPER_FACADE.map(from, to);
    }

    public static <S, D> D map(S from, Class<D> clazz) {
        return MAPPER_FACADE.map(from, clazz);
    }

    public static MapperFacade getMapperFacade() {
        return MAPPER_FACADE;
    }

    public static <S, D> List<D> mapAsList(Iterable<S> source, Class<D> destinationClass) {
        return MAPPER_FACADE.mapAsList(source, destinationClass);
    }

    private static class TrimConverter extends BidirectionalConverter<String, String> {
        @Override
        public String convertTo(String s, Type<String> type, MappingContext mappingContext) {
            return s.trim();
        }

        @Override
        public String convertFrom(String s, Type<String> type, MappingContext mappingContext) {
            return s.trim();
        }
    }


    private static class LocalDateTimeConverter extends BidirectionalConverter<LocalDateTime, LocalDateTime> {
        @Override
        public LocalDateTime convertTo(LocalDateTime source, Type<LocalDateTime> type, MappingContext mappingContext) {
            return LocalDateTime.from(source);
        }

        @Override
        public LocalDateTime convertFrom(LocalDateTime source, Type<LocalDateTime> type, MappingContext mappingContext) {
            return LocalDateTime.from(source);
        }
    }

    private static class LocalDateConverter extends BidirectionalConverter<LocalDate, LocalDate> {
        @Override
        public LocalDate convertTo(LocalDate source, Type<LocalDate> type, MappingContext mappingContext) {
            return LocalDate.from(source);
        }

        @Override
        public LocalDate convertFrom(LocalDate source, Type<LocalDate> type, MappingContext mappingContext) {
            return LocalDate.from(source);
        }
    }

    private static class LocalTimeConverter extends BidirectionalConverter<LocalTime, LocalTime> {
        @Override
        public LocalTime convertTo(LocalTime source, Type<LocalTime> type, MappingContext mappingContext) {
            return LocalTime.from(source);
        }

        @Override
        public LocalTime convertFrom(LocalTime source, Type<LocalTime> type, MappingContext mappingContext) {
            return LocalTime.from(source);
        }
    }
}