server:
  port: 8080

logging:
  level:
    root: warn
    ${cfg.appPackage}: info

spring:
  application:
    name: ${cfg.module}

  profiles:
    active: dev

    # springboot 2.4以后版本，官方推荐在application.yml(或application.yml)中配置所有的环境信息，不要在其他配置中再激活其他配置文件
    group:
      # 多个文件逗号分隔，例如想在dev环境导入application.mp.yml，application.mq-dev.yml两个文件，那么 dev: mp,mq-dev
      # 多个文件中有相同属性时，后面文件属性会覆盖前面的
      dev: mp
      prod: mp

  servlet:
    multipart:
      # maxFileSize 单个上传文件大小
      maxFileSize: 1024MB
      # maxRequestSize 是总数据大小
      maxRequestSize: 1024MB

task:
  thread:
    pool:
      corePoolSize: 5
      maxPoolSize: 15
      keepAliveSeconds: 30000
      queueCapacity: 5


<#if cfg.swagger3>
# springdoc-openapi项目配置
springdoc:
  swagger-ui:
    path: /swagger-ui.html
  api-docs:
    # 是否开启接口文档
    enabled: true
    path: /v3/api-docs
  group-configs:
    - group: 'demo接口文档'
      packages-to-scan: ${package.Controller}
# knife4j的增强配置，不需要增强可以不配
knife4j:
  enable: true
  setting:
    language: zh_cn
<#else>
</#if>

