package ${package.Controller};

import ${cfg.exceptionPackage}.AppException;
import ${cfg.commonPackage}.AppResp;
<#if cfg.swagger3>
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
</#if>
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用于测试而已哦
 * </p>
 *
 * @createTime: ${cfg.createTime}
 */
<#if cfg.swagger3>
@Tag(name = "测试控制器类")
</#if>
@Validated
@RestController("testController4${cfg.module}")
public class TestController {

<#if cfg.swagger3>
    @Operation(description = "测试异常1 ")
</#if>
    @GetMapping("/api/v1/test/test/error1")
    public AppResp<String> error1(@RequestParam(name = "x") int x) {
        System.out.println("x:" + x);
        int y = 1 / x;

        return AppResp.success(y + "");
    }


<#if cfg.swagger3>
    @Operation(description = "测试异常2 ")
</#if>
    @GetMapping("/api/v1/test/test/error2")
    public AppResp<String> error2() {
        throw new AppException("g嘎了");
    }


}