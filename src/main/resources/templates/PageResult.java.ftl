package ${cfg.commonPackage};

<#if cfg.swagger3>
import io.swagger.v3.oas.annotations.media.Schema;
</#if>
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * 分页结果
 */
<#if cfg.swagger3>
@Schema(name = "分页查询结果")
</#if>
public class PageResult<T> implements Serializable {

    <#if cfg.swagger3>
    @Schema(name =  "结果数据")
    </#if>
    private List<T> records = Collections.emptyList();

    <#if cfg.swagger3>
    @Schema(name =  "total", description = "总条数")
    </#if>
    private long total = 0;

    <#if cfg.swagger3>
    @Schema(name =  "pageSize", description = "每页条数")
    </#if>
    private long size = 10;

    <#if cfg.swagger3>
    @Schema(name = "pageNum", description = "当前页")
    </#if>
    private long current = 1;

    <#if cfg.swagger3>
    @Schema(name = "pages", description = "总页数")
    </#if>
    private long pages;

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getCurrent() {
        return current;
    }

    public void setCurrent(long current) {
        this.current = current;
    }

    public long getPages() {
        return pages;
    }

    public void setPages(long pages) {
        this.pages = pages;
    }
}