package ${cfg.configPackage};

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
//import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Collections;

/**
 * mybatis配置类
 */
@Configuration("mybatisPlusConfig4${cfg.module}")
@EnableTransactionManagement
@ComponentScan(basePackages = {"${package.Controller}"})
@MapperScan("${package.Mapper}")
public class MybatisPlusConfig {

    /**
     * 分页插件 3.5.X
     */
    @Bean
    public PaginationInnerInterceptor paginationInnerInterceptor() {
        PaginationInnerInterceptor paginationInterceptor = new PaginationInnerInterceptor();
        paginationInterceptor.setDbType(${cfg.dbType});
        // 设置最大单页限制数量，默认 500 条，-1 不受限制
        paginationInterceptor.setMaxLimit(100L);
        // 开启 count 的 join 优化,只针对部分 left join
        paginationInterceptor.setOptimizeJoin(true);
        return paginationInterceptor;
    }
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(){
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        mybatisPlusInterceptor.setInterceptors(Collections.singletonList(paginationInnerInterceptor()));
        return mybatisPlusInterceptor;
    }

    /**
    * 分页插件 3.2
    */
    //@Bean("paginationInterceptor4demo2")
    //public PaginationInterceptor paginationInterceptor() {
    //    PaginationInterceptor page = new PaginationInterceptor();
    //    // mysql类型：DbType.MYSQL.getDb()，支持的类型都在DbType这个枚举类中
    //    page.setDialectType(${cfg.dbType}.getDb());
    //    // 设置请求的页面大于最大页后操作， true调回到首页，false 继续请求  默认false
    //    // paginationInterceptor.setOverflow(false);
    //    // 设置最大单页限制数量，默认 500 条，-1 不受限制
    //    page.setLimit(1000);
    //    // 开启 count 的 join 优化,只针对部分 left join
    //    page.setCountSqlParser(new JsqlParserCountOptimize(true));
    //    return page;
    //}

}