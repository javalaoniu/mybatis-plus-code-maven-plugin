<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${package.Mapper}.${table.mapperName}">

<#if enableCache>
    <!-- 开启二级缓存 -->
    <cache type="org.mybatis.caches.ehcache.LoggingEhcache"/>

</#if>
<#if baseResultMap>
    <!-- 通用查询映射结果 -->
    <resultMap id="BaseResultMap" type="${package.Entity}.${entity}">
<#list table.fields as field>
<#if field.keyFlag><#--生成主键排在第一位-->
        <id column="${field.name}" property="${field.propertyName}" />
</#if>
</#list>
<#list table.commonFields as field><#--生成公共字段 -->
    <result column="${field.name}" property="${field.propertyName}" />
</#list>
<#list table.fields as field>
<#if !field.keyFlag><#--生成普通字段 -->
        <result column="${field.name}" property="${field.propertyName}" />
</#if>
</#list>
    </resultMap>

</#if>
<#if baseColumnList>
    <!-- 通用查询结果列 -->
    <sql id="Base_Column_List">
<#list table.commonFields as field>
        ${field.name},
</#list>
        ${table.fieldNames}
    </sql>
</#if>

    <insert id="batchInsert">
        insert into `${table.name}`(
        <#list table.fields as field>
        <#if field.keyFlag><#--生成主键排在第一位-->
            `${field.name}`,
        </#if>
        </#list>
        <#list table.commonFields as field><#--生成公共字段 -->
            `${field.name}`,
        </#list>
        <#list table.fields as field>
        <#if !field.keyFlag><#--生成普通字段 -->
            `${field.name}`<#sep>,
        </#if>
        </#list>

        ) values
        <foreach collection="list" item="entity" separator=",">
            (
            <#list table.fields as field>
            <#if field.keyFlag><#--生成主键排在第一位-->
                <#noparse>#{</#noparse>entity.${field.propertyName}<#noparse>}</#noparse>,
            </#if>
            </#list>
            <#list table.commonFields as field><#--生成公共字段 -->
                <#noparse>#{</#noparse>entity.${field.propertyName}<#noparse>}</#noparse>,
            </#list>
            <#list table.fields as field>
            <#if !field.keyFlag><#--生成普通字段 -->
                <#noparse>#{</#noparse>entity.${field.propertyName}<#noparse>}</#noparse><#sep>,
            </#if>
            </#list>

            )
        </foreach>
    </insert>

    <!--
    注意，除非表有一个PRIMARY KEY或UNIQUE索引，否则，使用以下三个语句没有意义，与使用单纯的INSERT INTO相同，每种方式都有一定的问题，自行判段是否使用。
    insert into 表示插入数据，数据库会检查主键，如果出现重复会报错；
    replace into 表示插入替换数据，需求表中有 PrimaryKey，或者 unique 索引，如果数据库已经存在数据，则先删除此行数据，然后插入新的数据，否则，直接插入新数据，如果没有数据效果则和 insert into 一样，必须具有delete和insert权限；
    insert ignore 表示，如果中已经存在相同的记录，则忽略当前新数据；
    insert on duplicate key update 如果在insert into 语句末尾指定了on duplicate key update，并且插入行后会导致在一个UNIQUE索引或PRIMARY KEY中出现重复值，则在出现重复值的行执行UPDATE；如果不会导致重复的问题，则插入新行，跟普通的insert into一样，具有insert和update权限
    -->
    <insert id="batchInsertOnDuplicateKeyUpdate">
        insert into `${table.name}`(
        <#list table.fields as field>
        <#if field.keyFlag><#--生成主键排在第一位-->
            `${field.name}`,
        </#if>
        </#list>
        <#list table.commonFields as field><#--生成公共字段 -->
            `${field.name}`,
        </#list>
        <#list table.fields as field>
        <#if !field.keyFlag><#--生成普通字段 -->
            `${field.name}`<#sep>,
        </#if>
        </#list>

        ) values
        <foreach collection="list" item="entity" separator=",">
            (
            <#list table.fields as field>
            <#if field.keyFlag><#--生成主键排在第一位-->
                <#noparse>#{</#noparse>entity.${field.propertyName}<#noparse>}</#noparse>,
            </#if>
            </#list>
            <#list table.commonFields as field><#--生成公共字段 -->
                <#noparse>#{</#noparse>entity.${field.propertyName}<#noparse>}</#noparse>,
            </#list>
            <#list table.fields as field>
            <#if !field.keyFlag><#--生成普通字段 -->
                <#noparse>#{</#noparse>entity.${field.propertyName}<#noparse>}</#noparse><#sep>,
            </#if>
            </#list>

            )
        </foreach>
        ON DUPLICATE KEY UPDATE
        <!--更新update_time这个字段，也可以自己定义其他字段-->
        `update_time` = now()
    </insert>

    <update id="batchModify">
        <foreach collection="list" item="item" separator=";" close=";">
            update
                `${table.name}`
            set
            <#list table.commonFields as field><#--生成公共字段 -->
                `${field.name}` = <#noparse>#{</#noparse>item.${field.propertyName}<#noparse>}</#noparse>,
            </#list>
            <#list table.fields as field>
            <#if !field.keyFlag><#--生成普通字段 -->
                `${field.name}` = <#noparse>#{</#noparse>item.${field.propertyName}<#noparse>}</#noparse><#sep>,
            </#if>
            </#list>

            where
            <#list table.fields as field>
            <#if field.keyFlag><#--生成主键排在第一位-->
                `${field.name}` = <#noparse>#{</#noparse>item.${field.propertyName}<#noparse>}</#noparse>
            </#if>
            </#list>
        </foreach>
    </update>

    <delete id="batchDelete">
        delete from `${table.name}` where id in
        <foreach collection="list" open="(" close=")" separator="," item="item">
        <#list table.fields as field>
        <#if field.keyFlag><#--生成主键排在第一位-->
            <#noparse>#{</#noparse>item.${field.name}<#noparse>}</#noparse>
        </#if>
        </#list>
        </foreach>
    </delete>
</mapper>
