package ${package.Service};

import com.baomidou.mybatisplus.extension.service.IService;
import ${cfg.commonPackage}.PageParams;
import ${cfg.commonPackage}.PageResult;
import ${cfg.commonPackage}.AppResp;
import ${cfg.voEntityPackage}.${entity}VO;
import ${package.Entity}.${entity};

import java.util.List;

<#assign entityIdType = "Long">
<#assign entityIdName = "Id">
<#list table.fields as field>
    <#if field.keyFlag>
    <#assign entityIdType = field.columnType>
    <#assign entityIdName = field.propertyName>
    </#if>
</#list>
/**
 * <p>
 * ${table.comment!} 业务层接口类
 * </p>
 *
 * @createTime: ${cfg.createTime}
 */
public interface ${entity}Service extends IService<${entity}> {

    AppResp<PageResult<${entity}VO>> page(PageParams<${entity}VO> pageParams);

    AppResp<${entity}VO> queryById(${'${entityIdType}'?capitalize} ${'${entity}'?uncap_first}${'${entityIdName}'?cap_first});

    AppResp<Void> add(${entity}VO ${'${entity}'?uncap_first}VO);

    AppResp<Void> delById(${'${entityIdType}'?capitalize} ${'${entity}'?uncap_first}Id);

    AppResp<Void> modify(${entity}VO ${'${entity}'?uncap_first}VO);

    AppResp<Void> batchAdd(List<${entity}VO> ${'${entity}'?uncap_first}Vos);

    AppResp<Void> batchModify(List<${entity}VO> ${'${entity}'?uncap_first}Vos);

    AppResp<Void> batchDelete(List<${entity}VO> ${'${entity}'?uncap_first}Vos);

}
