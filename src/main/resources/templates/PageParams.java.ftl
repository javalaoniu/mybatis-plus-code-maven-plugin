package ${cfg.commonPackage};

<#if cfg.swagger3>
import io.swagger.v3.oas.annotations.media.Schema;
</#if>

/**
 * 分页参数对象
 */
<#if cfg.swagger3>
@Schema(name = "分页查询参数")
</#if>
public class PageParams<T> {

    <#if cfg.swagger3>
    @Schema(name = "请求参数实体")
    </#if>
    private T entityParams;

    <#if cfg.swagger3>
    @Schema(name = "当前页，默认第1页")
    </#if>
    private long pageNum = 1;

    <#if cfg.swagger3>
    @Schema(name = "每页条数，默认每页10条")
    </#if>
    private long pageSize = 10;

    public T getEntityParams() {
        return entityParams;
    }

    public void setEntityParams(T entityParams) {
        this.entityParams = entityParams;
    }

    public long getPageNum() {
        if(pageNum<1){
            pageNum = 1;
        }
        return pageNum;
    }

    public void setPageNum(long pageNum) {
        if(pageNum < 1){
            pageNum = 1;
        }
        this.pageNum = pageNum;
    }

    public long getPageSize() {
        if(pageSize<1){
            pageSize = 10;
        }
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        if(pageSize < 1){
            pageSize = 10;
        }
        this.pageSize = pageSize;
    }
}