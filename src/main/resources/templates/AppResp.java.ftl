package ${cfg.commonPackage};

<#if cfg.swagger3>
import io.swagger.v3.oas.annotations.media.Schema;
</#if>
import java.io.Serializable;

/**
* 返回类型封装
*/
<#if cfg.swagger3>
@Schema(name = "响应报文体的包装")
</#if>
public class AppResp<T> implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final int SUCCESS = 200;
    public static final String SUCCESS_MSG = "success";
    public static final int FAILURE = -1;
    public static final String FAILURE_MSG = "fail";

    <#if cfg.swagger3>
    @Schema(requiredMode = Schema.RequiredMode.REQUIRED, name = "操作结果，成功0、失败-1或其他非0在状态码")
    </#if>
    private int code;
    <#if cfg.swagger3>
    @Schema(requiredMode = Schema.RequiredMode.NOT_REQUIRED, name = "请求成功或失败时返回的提示信息")
    </#if>
    private String msg;
    <#if cfg.swagger3>
    @Schema(requiredMode = Schema.RequiredMode.NOT_REQUIRED, name = "响应的业务数据")
    </#if>
    private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static <T> AppResp<T> success() {
        AppResp<T> entity = new AppResp<>();
        entity.setCode(SUCCESS);
        entity.setMsg(SUCCESS_MSG);
        return entity;
    }

    public static <T> AppResp<T> success(T data) {
        AppResp<T> entity = success();
        entity.setData(data);
        entity.setMsg(SUCCESS_MSG);
        return entity;
    }

    public static <T> AppResp<T> error(String message) {
        AppResp<T> entity = new AppResp<>();
        entity.setCode(FAILURE);
        entity.setMsg(message);
        return entity;
    }

    public static <T> AppResp<T> error(int code, String message) {
        AppResp<T> entity = new AppResp<>();
        entity.setCode(code);
        entity.setMsg(message);
        return entity;
    }
}